/*
	Esp32 Websockets Client

	This sketch:
        1. Connects to a WiFi network
        2. Connects to a Websockets server
        3. Sends the websockets server a message ("Hello Server")
        4. Prints all incoming messages while the connection is open

	Hardware:
        For this sketch you only need an ESP32 board.

	Created 15/02/2019
	By Gil Maimon
	https://github.com/gilmaimon/ArduinoWebsockets

*/

#include <ArduinoWebsockets.h>
#include <WiFi.h>

const char* ssid = "BTHub5-K2HT"; //Enter SSID
const char* password = "ea235acf68"; //Enter Password
const char* websockets_server_host = "ws://node-red-pager.eu-gb.mybluemix.net/mohammad"; //Enter server adress
const uint16_t websockets_server_port = 80; // Enter server port

uint8_t speakerPin = 23;
int length = 15; // the number of notes
char notes[] = "ccggaagffeeddc "; // a space represents a rest
int beats[] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4 };
int tempo = 300;
using namespace websockets;
WebsocketsClient client;

////////////////////////////////OLED////////////////////////////////
#include "Arduino.h"
#include "heltec.h"


////////////////////////////////OLED-END////////////////////////////////

void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(speakerPin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(speakerPin, LOW);
    delayMicroseconds(tone);
  }
}

void playNote(char note, int duration) {
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
  int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };

  // play the tone corresponding to the note name
  for (int i = 0; i < 8; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}

void setOledText(String message)
{
//  int cursorPoint = display.width();
//  int maxPoint = -12 * message.length();
//  while(true)
//  {
//    display.clearDisplay();
//
//    display.setTextSize(2);
//    display.setTextColor(WHITE);
//    display.setCursor(cursorPoint, 10);
//    display.setTextWrap(false);
//    // Display static text
//    display.print(message);
//    display.display();
//    delay(100);
//    cursorPoint = cursorPoint - 4;
//    if (cursorPoint < maxPoint) cursorPoint = display.width();
//    if (client.poll()) break;
//  }
}

void drawTest(String message)
{
    int cursorPoint = 128;
    int maxPoint = -6 * message.length();
    while(true){
      Heltec.display->setLogBuffer(30, 300);
      Heltec.display->clear();
  
      //Heltec.display->setTextSize(2);
      //Heltec.display->setTextColor(WHITE);
      //Heltec.display->setCursor(0, 10);
      //Heltec.display->setTextWrap(false);
      // Display static text
      Heltec.display->drawString(cursorPoint,0,message);
      Heltec.display->drawLogBuffer(0, 0);
      Heltec.display->display();
      delay(100);
      cursorPoint = cursorPoint - 4;
      if (cursorPoint < maxPoint) cursorPoint = 128;
      if (client.poll()) break;
    }
}

void setup() {
    Serial.begin(115200);
    // Connect to wifi
    WiFi.begin(ssid, password);

    // Wait some time to connect to wifi
    for(int i = 0; i < 10 && WiFi.status() != WL_CONNECTED; i++) {
        Serial.print(".");
        delay(1000);
    }

    // Check if connected to wifi
    if(WiFi.status() != WL_CONNECTED) {
        Serial.println("No Wifi!");
        return;
    }

    Serial.println("Connected to Wifi, Connecting to server.");
    // try to connect to Websockets server
    bool connected = client.connect(websockets_server_host);
    if(connected) {
        Serial.println("Connected!");
        client.send("Hello Server");
    } else {
        Serial.println("Not Connected!");
    }
    
    // run callback when messages are received
    client.onMessage([&](WebsocketsMessage message) {
        Serial.print("Got Message: ");
        Serial.println(message.data());
        for (int i = 0; i < length; i++) {
        if (notes[i] == ' ') {
          delay(beats[i] * tempo); // rest
        }
        else {
          playNote(notes[i], beats[i] * tempo);
        }
        
          // pause between notes
          delay(tempo / 2); 
        }
        drawTest(message.data());
        
    });
    pinMode(speakerPin, OUTPUT);
    ////////OLED setup/////////////////////    
    Heltec.begin(true /*DisplayEnable Enable*/, false /*LoRa Disable*/, true /*Serial Enable*/);
    Heltec.display->setContrast(255);
    Heltec.display->setFont(ArialMT_Plain_10);
    ////////OLED setup end/////////////////////
}

void loop() {
    // let the websockets client check for incoming messages
    if(client.available()) {
        client.poll();
    }
    delay(500);
}
